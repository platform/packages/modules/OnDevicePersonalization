/*
 * Copyright (C) 2024 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.ondevicepersonalization.services.data.errors;

import android.annotation.NonNull;

import com.android.ondevicepersonalization.internal.util.AnnotationValidations;
import com.android.ondevicepersonalization.internal.util.DataClass;

@DataClass(genBuilder = true, genEqualsHashCode = true)
public class ErrorData {

    /** The error code returned by the {@code IsolatedService}. */
    @NonNull private final int mErrorCode;

    /** The aggregated count of {@link #mErrorCode} on the given {@link #mEpochDay}. */
    @NonNull private final int mErrorCount;

    /** The date associated with this record of aggregated errors. */
    @NonNull private final int mEpochDay;

    /** The version of the package of the {@code IsolatedService}. */
    @NonNull private final long mServicePackageVersion;

    // Code below generated by codegen v1.0.23.
    //
    // DO NOT MODIFY!
    // CHECKSTYLE:OFF Generated code
    //
    // To regenerate run:
    // $ codegen
    // $ANDROID_BUILD_TOP/packages/modules/OnDevicePersonalization/src/com/android/ondevicepersonalization/services/data/errors/ErrorData.java
    //
    // To exclude the generated code from IntelliJ auto-formatting enable (one-time):
    //   Settings > Editor > Code Style > Formatter Control
    // @formatter:off

    @DataClass.Generated.Member
    /* package-private */ ErrorData(
            @NonNull int errorCode,
            @NonNull int errorCount,
            @NonNull int epochDay,
            @NonNull long servicePackageVersion) {
        this.mErrorCode = errorCode;
        AnnotationValidations.validate(NonNull.class, null, mErrorCode);
        this.mErrorCount = errorCount;
        AnnotationValidations.validate(NonNull.class, null, mErrorCount);
        this.mEpochDay = epochDay;
        AnnotationValidations.validate(NonNull.class, null, mEpochDay);
        this.mServicePackageVersion = servicePackageVersion;
        AnnotationValidations.validate(NonNull.class, null, mServicePackageVersion);

        // onConstructed(); // You can define this method to get a callback
    }

    @DataClass.Generated.Member
    public @NonNull int getErrorCode() {
        return mErrorCode;
    }

    @DataClass.Generated.Member
    public @NonNull int getErrorCount() {
        return mErrorCount;
    }

    @DataClass.Generated.Member
    public @NonNull int getEpochDay() {
        return mEpochDay;
    }

    @DataClass.Generated.Member
    public @NonNull long getServicePackageVersion() {
        return mServicePackageVersion;
    }

    @Override
    @DataClass.Generated.Member
    public boolean equals(@android.annotation.Nullable Object o) {
        // You can override field equality logic by defining either of the methods like:
        // boolean fieldNameEquals(ErrorData other) { ... }
        // boolean fieldNameEquals(FieldType otherValue) { ... }

        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        @SuppressWarnings("unchecked")
        ErrorData that = (ErrorData) o;
        //noinspection PointlessBooleanExpression
        return true
                && mErrorCode == that.mErrorCode
                && mErrorCount == that.mErrorCount
                && mEpochDay == that.mEpochDay
                && mServicePackageVersion == that.mServicePackageVersion;
    }

    @Override
    @DataClass.Generated.Member
    public int hashCode() {
        // You can override field hashCode logic by defining methods like:
        // int fieldNameHashCode() { ... }

        int _hash = 1;
        _hash = 31 * _hash + mErrorCode;
        _hash = 31 * _hash + mErrorCount;
        _hash = 31 * _hash + mEpochDay;
        _hash = 31 * _hash + Long.hashCode(mServicePackageVersion);
        return _hash;
    }

    /** A builder for {@link ErrorData} */
    @SuppressWarnings("WeakerAccess")
    @DataClass.Generated.Member
    public static class Builder {

        private @NonNull int mErrorCode;
        private @NonNull int mErrorCount;
        private @NonNull int mEpochDay;
        private @NonNull long mServicePackageVersion;

        private long mBuilderFieldsSet = 0L;

        public Builder(
                @NonNull int errorCode,
                @NonNull int errorCount,
                @NonNull int epochDay,
                @NonNull long servicePackageVersion) {
            mErrorCode = errorCode;
            AnnotationValidations.validate(NonNull.class, null, mErrorCode);
            mErrorCount = errorCount;
            AnnotationValidations.validate(NonNull.class, null, mErrorCount);
            mEpochDay = epochDay;
            AnnotationValidations.validate(NonNull.class, null, mEpochDay);
            mServicePackageVersion = servicePackageVersion;
            AnnotationValidations.validate(NonNull.class, null, mServicePackageVersion);
        }

        @DataClass.Generated.Member
        public @NonNull Builder setErrorCode(@NonNull int value) {
            checkNotUsed();
            mBuilderFieldsSet |= 0x1;
            mErrorCode = value;
            return this;
        }

        @DataClass.Generated.Member
        public @NonNull Builder setErrorCount(@NonNull int value) {
            checkNotUsed();
            mBuilderFieldsSet |= 0x2;
            mErrorCount = value;
            return this;
        }

        @DataClass.Generated.Member
        public @NonNull Builder setEpochDay(@NonNull int value) {
            checkNotUsed();
            mBuilderFieldsSet |= 0x4;
            mEpochDay = value;
            return this;
        }

        @DataClass.Generated.Member
        public @NonNull Builder setServicePackageVersion(@NonNull long value) {
            checkNotUsed();
            mBuilderFieldsSet |= 0x8;
            mServicePackageVersion = value;
            return this;
        }

        /** Builds the instance. This builder should not be touched after calling this! */
        public @NonNull ErrorData build() {
            checkNotUsed();
            mBuilderFieldsSet |= 0x10; // Mark builder used

            ErrorData o = new ErrorData(mErrorCode, mErrorCount, mEpochDay, mServicePackageVersion);
            return o;
        }

        private void checkNotUsed() {
            if ((mBuilderFieldsSet & 0x10) != 0) {
                throw new IllegalStateException(
                        "This Builder should not be reused. Use a new Builder instance instead");
            }
        }
    }

    @DataClass.Generated(
            time = 1724390597119L,
            codegenVersion = "1.0.23",
            sourceFile =
                    "packages/modules/OnDevicePersonalization/src/com/android/ondevicepersonalization/services/data/errors/ErrorData.java",
            inputSignatures =
                    "private final @android.annotation.NonNull int mErrorCode\n"
                        + "private final @android.annotation.NonNull int mErrorCount\n"
                        + "private final @android.annotation.NonNull int mEpochDay\n"
                        + "private final @android.annotation.NonNull long mServicePackageVersion\n"
                        + "class ErrorData extends java.lang.Object implements []\n"
                        + "@com.android.ondevicepersonalization.internal.util.DataClass(genBuilder=true,"
                        + " genEqualsHashCode=true)")
    @Deprecated
    private void __metadata() {}

    // @formatter:on
    // End of generated code

}
