/*
 * Copyright (C) 2022 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.ondevicepersonalization.services.data.events;

import android.provider.BaseColumns;

/** Contract for the queries table. Defines the table. */
public class QueriesContract {
    private QueriesContract() {
    }

    /**
     * Table containing queries. Each row in the table represents a single query.
     */
    public static class QueriesEntry implements BaseColumns {
        public static final String TABLE_NAME = "queries";

        /** The id of the query. */
        public static final String QUERY_ID = "queryId";

        /** Time of the query in milliseconds. */
        public static final String TIME_MILLIS = "timeMillis";

        /** Name of the app that invoked ODP. */
        public static final String APP_PACKAGE_NAME = "appPackageName";

        /** Component Name of the service that handled the request */
        public static final String SERVICE_NAME = "serviceName";

        /** Hash of the signing key of the service. */
        public static final String SERVICE_CERT_DIGEST = "serviceCertDigest";

        /** Blob representing the common query fields. */
        public static final String QUERY_DATA = "queryData";

        public static final String CREATE_TABLE_STATEMENT =
                "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + " ("
                    + QUERY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                    + TIME_MILLIS + " INTEGER NOT NULL,"
                    + APP_PACKAGE_NAME + " TEXT NOT NULL,"
                    + SERVICE_NAME + " TEXT NOT NULL,"
                    + SERVICE_CERT_DIGEST + " TEXT NOT NULL,"
                    + QUERY_DATA + " BLOB NOT NULL)";

        public static final String UPGRADE_V1_TO_V2_STATEMENT =
                "ALTER TABLE " + TABLE_NAME
                + " ADD COLUMN " + APP_PACKAGE_NAME + " TEXT NOT NULL DEFAULT ''";

        public static final String UPGRADE_V2_TO_V3_STATEMENT =
                "ALTER TABLE " + TABLE_NAME
                + " ADD COLUMN " + SERVICE_CERT_DIGEST + " TEXT NOT NULL DEFAULT ''";

        private QueriesEntry() {}
    }
}
